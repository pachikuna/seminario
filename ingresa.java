import java.util.Scanner;
public class ingresa{
  public static Scanner teclado;
  public static void main(String args[ ]) {
    teclado = new Scanner(System.in);
    long binario = leeBinario();
    int decimal = convierteDecimal(binario);
    imprime(binario,decimal);
  }
  
  public static long leeBinario(){
    String n = "";
    System.out.print("Ingrese numero binario: ");
    n = teclado.nextLine().trim();
    
    return Long.parseLong(n);
  }
  
  public static int convierteDecimal(long bi){
    String b = String.valueOf(bi);
    int largo = b.length();
    int decimal = 0;
    int des = 0;
    for(int i = largo-1; i >= 0; i--){
      des = (int)(bi/Math.pow(10,i));
      decimal = decimal + des*(int)Math.pow(2,i);
      bi = (long)(bi%Math.pow(10,i));
    }
    return decimal;
  }
  
  public static void imprime(long bi, int decimal){
    System.out.println("El equivalente decimal del binario "+bi+" es: "+decimal);
  }
}
