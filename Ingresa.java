package test;

import java.util.Scanner;
/**
 * 
 *
 * @author Este es un comentario
 */
public class Ingresa{ 
	 /**
	  * 
	  *
	  * @author Este es un comentario
	  */
  public static Scanner teclado;
  /**
   * 
   *
   * @author Este es un comentario
   */
  public static void main(final String args[ ]) { //Aqui esta el comentario
    teclado = new Scanner(System.in);
    final long binario = leeBinario();
    final int decimal = convierteDecimal(binario);
    imprime(binario,decimal);
  }
  /**
   * 
   *
   * @author Este es un comentario
   */
  public static long leeBinario(){ 
    //String nombre = "";
    System.out.print("Ingrese numero binario: ");
    final String nombre = teclado.nextLine().trim();
    
    return Long.parseLong(nombre);
  }
  /**
  * 
  *
  * @author Este es un comentario
  */
  public static int convierteDecimal(long binar){ //Aqui esta el comentario // NOPMD by laboratorio on 27-04-17 14:41
    long binar2 = binar;
	final String var = String.valueOf(binar2);
    final int largo = var.length();
    int decimal=0;

    for(int i = largo-1; i >= 0; i--){
      final int des = (int)(binar2/Math.pow(10,i));
      decimal = decimal + des*(int)Math.pow(2,i);
      binar2 = (long)(binar2%Math.pow(10,i));
    }
    return decimal;
  }
  /**
   * 
   *
   * @author Este es un comentario
   */
  public static void imprime(final long binar, final int decimal){ 
    System.out.println("El equivalente decimal del binario "+binar+" es: "+decimal);
  }
}
